// z3a9.cpp
//
// Copyright (C) 2011, 2012, Bátfai Norbert, nbatfai@inf.unideb.hu, nbatfai@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// Ez a program szabad szoftver; terjeszthetõ illetve módosítható a
// Free Software Foundation által kiadott GNU General Public License
// dokumentumában leírtak; akár a licenc 3-as, akár (tetszõleges) késõbbi
// változata szerint.
//
// Ez a program abban a reményben kerül közreadásra, hogy hasznos lesz,
// de minden egyéb GARANCIA NÉLKÜL, az ELADHATÓSÁGRA vagy VALAMELY CÉLRA
// VALÓ ALKALMAZHATÓSÁGRA való származtatott garanciát is beleértve.
// További részleteket a GNU General Public License tartalmaz.
//
// A felhasználónak a programmal együtt meg kell kapnia a GNU General
// Public License egy példányát; ha mégsem kapta meg, akkor
// tekintse meg a <http://www.gnu.org/licenses/> oldalon.

#include <iostream>
#include <cmath>
#include <fstream>
#include <stdio.h>

#include "Csomopont.hpp"

class LZWBinFa
{
public:
    LZWBinFa()
    {
        gyoker = new Csomopont('/');
        fa = gyoker;
    }

    LZWBinFa(const LZWBinFa &forras):LZWBinFa()
    {
        std::cout << "copy ctor" << std::endl;

        if (gyoker != nullptr)
        {
            szabadit(gyoker);

            gyoker = copy(forras.gyoker, forras.fa);
        }
    }
    
    LZWBinFa(LZWBinFa &&forras)
    {
        std::cout << "Move ctor" << std::endl;

        gyoker = nullptr;

        *this = std::move(forras);
    }

    LZWBinFa& operator=(const LZWBinFa &forras)
    {
        if (this == &forras)
        {
            return *this;
        }
        
        if (forras.gyoker == nullptr)
        {
            return *this;
        }

        szabadit(gyoker);

        gyoker = copy(forras.gyoker,forras.fa);

        std::cout << "Copy assignment" << std::endl;

        return *this;
    }

    LZWBinFa& operator=(LZWBinFa &&forras)
    {
        std::cout << "Move assignment " << std::endl;
        std::swap(gyoker, forras.gyoker);
        return *this;
    }

    ~LZWBinFa()
    {
        szabadit (gyoker);
    }

    void operator<<(const char b)
    {
        if (b == '0')
        {
            if (!fa->nullasGyermek())
            {
                Csomopont *uj = new Csomopont('0');

                fa->ujNullasGyermek(uj);

                fa = gyoker;
            }
            else
            {
                fa = fa->nullasGyermek();
            }
        }
        else
        {
            if (!fa->egyesGyermek())
            {
                Csomopont *uj = new Csomopont('1');
                fa->ujEgyesGyermek (uj);
                fa = gyoker;
            }
            else
            {
                fa = fa->egyesGyermek();
            }
        }
    }
    


    void kiir()
    {
        melyseg = 0;

        kiir(gyoker, std::cout);
    }

    void kiir (std::ostream &os)
    {
        melyseg = 0;

        kiir(gyoker, os);
    }

    int getMelyseg();
    double getAtlag();
    double getSzoras();
    int getdarab(int* out);

    friend std::ostream &operator<<(std::ostream &os, LZWBinFa &bf)
    {
        bf.kiir(os);

        return os;
    }



private:
    Csomopont *fa;

    int melyseg, atlagosszeg, atlagdb;
    double szorasosszeg;

    void kiir(Csomopont *elem, std::ostream & os)
    {
        if (elem != nullptr)
        {
            // InOrder
            ++melyseg;
            kiir(elem->egyesGyermek(), os);
     
            for (int i = 0; i < melyseg; ++i)
            {
                os << "---";
            }

            os << elem->getBetu() << "(" << melyseg - 1 << ")" << std::endl;
            
            kiir(elem->nullasGyermek(), os);

            --melyseg;
        }
    }




    Csomopont *copy (const Csomopont *forras, const Csomopont *regifa )
    {
        Csomopont* masolt = nullptr;

        if (forras != nullptr)
        {
            masolt = new Csomopont(forras->getBetu());
            
            masolt->ujEgyesGyermek(copy(forras->egyesGyermek(), regifa));
            
            masolt->ujNullasGyermek(copy(forras->nullasGyermek(),regifa));
            
            if (regifa == forras)
            {
                fa = masolt;
            }
        }

        return masolt;
    }

    void szabadit (Csomopont *elem)
    {
        if (elem != nullptr)
        {
            // Free the children, then ourselves.
            // PostOrder
            szabadit(elem->egyesGyermek());
            szabadit(elem->nullasGyermek());

            delete elem;
        }
    }

protected:
    Csomopont *gyoker;

    int maxMelyseg;
    double atlag, szoras;

    void rmelyseg(Csomopont *elem);
    void ratlag(Csomopont *elem);
    void rszoras(Csomopont *elem);
    void rdarab(Csomopont *elem, int* out);
};


int LZWBinFa::getMelyseg()
{
    melyseg = maxMelyseg = 0;
    rmelyseg(gyoker);

    return maxMelyseg - 1;
}

int LZWBinFa::getdarab(int* out){

	rdarab(gyoker,out);
}



double LZWBinFa::getAtlag()
{
    melyseg = atlagosszeg = atlagdb = 0;

    ratlag(gyoker);
    atlag = ((double) atlagosszeg) / atlagdb;

    return atlag;
}

double LZWBinFa::getSzoras()
{
    atlag = getAtlag();

    szorasosszeg = 0.0;
    melyseg = atlagdb = 0;

    rszoras(gyoker);

    if ((atlagdb) - 1 > 0)
    {
        szoras = std::sqrt (szorasosszeg / (atlagdb - 1));
    }
    else
    {
        szoras = std::sqrt (szorasosszeg);
    }

    return szoras;
}

void LZWBinFa::rmelyseg(Csomopont *elem)
{
    if (elem != nullptr)
    {
        ++melyseg;

        if (melyseg > maxMelyseg)
        {
            maxMelyseg = melyseg;
        }

        rmelyseg(elem->egyesGyermek());
        rmelyseg(elem->nullasGyermek());

        --melyseg;
    }
}

void LZWBinFa::rdarab(Csomopont *elem, int* out)
{
    if (elem != nullptr)
    {
	(*out)++;
        rdarab(elem->egyesGyermek(),out);
        rdarab(elem->nullasGyermek(),out);
    }
}

void LZWBinFa::ratlag(Csomopont *elem)
{
    if (elem != nullptr)
    {
        ++melyseg;

        ratlag(elem->egyesGyermek());
        ratlag(elem->nullasGyermek());
        --melyseg;

        if ((elem->egyesGyermek() == nullptr) && (elem->nullasGyermek() == nullptr))
        {
            ++atlagdb;
            atlagosszeg += melyseg;
        }
    }
}

void LZWBinFa::rszoras (Csomopont *elem)
{
    if (elem != nullptr)
    {
        ++melyseg;

        rszoras(elem->egyesGyermek());
        rszoras(elem->nullasGyermek());

        --melyseg;
        if ((elem->egyesGyermek() == nullptr) && (elem->nullasGyermek() == nullptr))
        {
            ++atlagdb;
            szorasosszeg += ((melyseg - atlag) * (melyseg - atlag));
        }
    }
}

void usage()
{
    std::cout << "Usage: lzwtree in_file -o out_file" << std::endl;
}





int main (int argc, char **argv)
{

    if (argc != 4)
    {
        usage ();

        return -1;
    }


    char *inFile = *++argv;

    if (*((*++argv) + 1) != 'o')
    {
        usage();
        return -2;
    }

    std::fstream beFile(inFile, std::ios_base::in);

    if (!beFile)
    {
        std::cout << inFile << " nem letezik..." << std::endl;

        usage();

        return -3;
    }

    std::fstream kiFile(*++argv, std::ios_base::out);

    unsigned char b;
    LZWBinFa binFa;

    

    bool kommentben = false;

    while (beFile.read((char *) &b, sizeof (unsigned char)))
    {
        if (b == 0x3e)
        {
            kommentben = true;
            continue;
        }

        if (b == 0x0a)
        {
            kommentben = false;
            continue;
        }

        if (kommentben) 
        {
            continue;
        }

        if (b == 0x4e)
        {
            continue;
        }

        for (int i = 0; i < 8; ++i)
        {
            if (b & 0x80)
            {
                binFa << '1';
            }
            else
            {
                binFa << '0';
            }

            b <<= 1;
        }
    }

    //LZWBinFa binFa_copy = binFa; 

    //kiFile << binFa;
    LZWBinFa egyesfa;
    LZWBinFa nullasfa;

	for(int i = 0; i <15 ; i++){
		egyesfa << '1';
		nullasfa << '0';
	}

/*
    kiFile << "depth = " << binFa.getMelyseg () << std::endl;
    kiFile << "mean = " << binFa.getAtlag () << std::endl;
    kiFile << "var = " << binFa.getSzoras () << std::endl;

    kiFile<<"##########################másolt##############################\n";
    kiFile << binFa_copy;

    kiFile << "depth = " << binFa_copy.getMelyseg () << std::endl;
    kiFile << "mean = " << binFa_copy.getAtlag () << std::endl;
    kiFile << "var = " << binFa_copy.getSzoras () << std::endl;
    
    LZWBinFa binFa_move = std::move(binFa_copy);

    kiFile<<"##########################mozgatott##############################\n";
    kiFile << binFa_move;
    kiFile << "depth = " << binFa_move.getMelyseg () << std::endl;
    kiFile << "mean = " << binFa_move.getAtlag () << std::endl;
    kiFile << "var = " << binFa_move.getSzoras () << std::endl;
*/

int* darabszam;
*darabszam = 0;

 egyesfa.getdarab(darabszam);


    kiFile << "Fa 5db nullás elemmel: "<< std::endl << nullasfa << std::endl;

    kiFile << "Fa 5db egyes elemmel: " << std::endl << egyesfa << std::endl << "Elemeik száma gyökérelemmel: " ;

kiFile << *darabszam<< std::endl;

    kiFile.close ();
    beFile.close ();


    return 0;
}
