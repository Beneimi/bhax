

/**
 * Class Szoba_Cica
 */
public class Szoba_Cica extends Cica {

	//
	// Fields
	//

  private boolean van_alom;
  
	//
	// Constructors
	//
	public Szoba_Cica (String sz�ne, String neve, Double kora) {
		super(sz�ne,neve,kora);
		this.van_alom = false;
	}
	//
	// Methods
	//


	//
	// Accessor methods
	//

	/**
	 * Set the value of van_alom
	 * @param newVar the new value of van_alom
	 */
  private void setVan_alom (boolean newVar) {
  	van_alom = newVar;
  }

	/**
	 * Get the value of van_alom
	 * @return the value of van_alom
	 */
  private boolean getVan_alom () {
  	return van_alom;
  }

	//
	// Other methods
	//

	/**
	 */
  public void AlmotAd()
  {
	  this.van_alom = true;
  }



}
