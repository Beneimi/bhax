

/**
 * Class Cica
 */
public class Cica {

	//
	// Fields
	//

  protected String sz�n;
  protected String n�v;
  protected Double kor;
  
	//
	// Constructors
	//
	public Cica (String sz�ne, String neve, Double kora ) {
		this.kor = kora;
		this.n�v = neve;
		this.sz�n = sz�ne;
	};
  
	//
	// Methods
	//


	//
	// Accessor methods
	//

	/**
	 * Set the value of sz�n
	 * @param newVar the new value of sz�n
	 */
  private void setSz�n (String newVar) {
  	sz�n = newVar;
  }

	/**
	 * Get the value of sz�n
	 * @return the value of sz�n
	 */
  private String getSz�n () {
  	return sz�n;
  }

	/**
	 * Set the value of n�v
	 * @param newVar the new value of n�v
	 */
  private void setN�v (String newVar) {
  	n�v = newVar;
  }

	/**
	 * Get the value of n�v
	 * @return the value of n�v
	 */
  private String getN�v () {
  	return n�v;
  }

	/**
	 * Set the value of kor
	 * @param newVar the new value of kor
	 */
  private void setKor (Double newVar) {
  	kor = newVar;
  }

	/**
	 * Get the value of kor
	 * @return the value of kor
	 */
  private Double getKor () {
  	return kor;
  }

	//
	// Other methods
	//

	/**
	 */
  public void Ny�vog()
  {
	  System.out.println("Meow");
	}


	/**
	 * @return       boolean
	 * @param        �ldozat
	 */
  public boolean Megesz(Eg�r �ldozat)
  {
	  if(�ldozat.M�ret() < 5) {
		  �ldozat.�l� = false;
		  System.out.println("Nyam-nyam");
	  	  return true;
	  }
	  else {
		  System.out.println("T�l nagy falat");
		  return false;
	  }
	}


  public static void main(String args[]) {
	  
	  Cica szederke = new Cica("sz�rke","Szederke",1.0);
	  Eg�r kiseg�r = new Eg�r(3);
	  szederke.Ny�vog();
	  szederke.Megesz(kiseg�r);

  }


}
