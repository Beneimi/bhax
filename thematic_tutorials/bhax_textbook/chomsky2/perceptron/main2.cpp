#include <iostream>
#include "perceptron.hpp"
#include "png++/png.hpp"
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include "png++/rgb_pixel.hpp"

int main (int argc, char **argv)
{
  srand (time(NULL));

    png::image <png::rgb_pixel> png_image (argv[1]);

    int size = png_image.get_width() * png_image.get_height();
    
    Perceptron* p = new Perceptron (3, size, 256, size);
    
    double* image = new double[size];    
    double* newPNG = new double[size];
    /*
    for (int i = 0; i<png_image.get_width(); ++i)
        for (int j = 0; j<png_image.get_height(); ++j)
            image[i*png_image.get_width() + j] = png_image[i][j].red;




    for(int i = 0; i < png_image.get_width(); ++i)
        for(int j = 0; j<png_image.get_height(); ++j)
            png_image[i][j].red = newPNG[j*png_image.get_width()+j];

*/
			png::rgb_pixel inner_color = png::rgb_pixel(0, 0, 255);
			png::rgb_pixel outer_color = png::rgb_pixel(255, 0, 0);
			png::rgb_pixel black = png::rgb_pixel(255, 255, 255);

    for (int i = 0; i<png_image.get_width(); ++i){
        for (int j = 0; j<png_image.get_height(); ++j){
		if(png_image[i][j].red < 250  ){

			png_image[i][j] = black;
			std::cout << "r[" << i << "," << j << "]" << std::endl;

		}
		else{
			png_image[i][j] = outer_color;
			//png_image[i][j].green = rand()%255;
			//png_image[i][j].red = rand()%255;
		}
	}
     }
		

    png_image.write("output.png");   
  

    double value = (*p) (image);
    
    std::cout << value << std::endl;
    
    delete p;
    delete [] image;
    
}
