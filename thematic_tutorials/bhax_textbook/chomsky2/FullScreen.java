import java.awt.*;
import java.awt.event.*;  
import java.util.Random;


import javax.swing.JButton;
import javax.swing.JFrame;

public class FullScreen {
	

	public static void main(String[] args) {
		Random rand = new Random();
		   GraphicsEnvironment ge = GraphicsEnvironment.
				   getLocalGraphicsEnvironment();
				   GraphicsDevice[] gs = ge.getScreenDevices();
				   for (int j = 0; j < gs.length; j++) {
				      GraphicsDevice gd = gs[j];
				      GraphicsConfiguration[] gc =
				      gd.getConfigurations();
				         JFrame f = new
				         JFrame(gs[j].getDefaultConfiguration());
				      for (int i=0; i < gc.length; i++) {
				         Canvas c = new Canvas(gc[i]);
				         Rectangle gcBounds = gc[i].getBounds();
				         int xoffs = gcBounds.x;
				         int yoffs = gcBounds.y;
				         f.getContentPane().add(c);
				         f.setLocation(0, 0); 

				      }
					     JButton b=new JButton("Give Me a Color!");  
					     b.setBounds(100,100,140, 40);  

							f.add(b); 
							f.setSize(1920,1080);    
							f.setLayout(null);    
							f.setVisible(true);    
							f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						    gd.setFullScreenWindow(f);

							 b.addActionListener(new ActionListener(){  
								    public void actionPerformed(ActionEvent e){  
								    	f.getContentPane().setBackground(new java.awt.Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255)));;
								    }  
								    });  
				   }
				   
	}
	
}