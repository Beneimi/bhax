import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class LeetCipher {
	
	protected String[][] chars = new String[26][3];

	public LeetCipher() {

		
		chars[0][0] = "a";
		chars[1][0] = "b";
		chars[2][0] = "c";
		chars[3][0] = "d";
		chars[4][0] = "e";
		chars[5][0] = "f";
		chars[6][0] = "g";
		chars[7][0] = "h";
		chars[8][0] = "i";
		chars[9][0] = "j";
		chars[10][0] = "k";
		chars[11][0] = "l";
		chars[12][0] = "m";
		chars[13][0] = "n";
		chars[14][0] = "o";
		chars[15][0] = "p";
		chars[16][0] = "q";
		chars[17][0] = "r";
		chars[18][0] = "s";
		chars[19][0] = "t";
		chars[20][0] = "u";
		chars[21][0] = "v";
		chars[22][0] = "w";
		chars[23][0] = "x";
		chars[24][0] = "y";
		chars[25][0] = "z";
		
		chars[0][2] = "4";	chars[0][1] = "@";
		chars[1][2] = "8";	chars[1][1] = "|3";
		chars[2][2] = "©";	chars[2][1] = "Ȼ";
		chars[3][2] = "|)";	chars[3][1] = "|}";
		chars[4][2] = "3";	chars[4][1] = "€";
		chars[5][2] = "|=";	chars[5][1] = "ph";
		chars[6][2] = "ℊ";	chars[6][1] = "Ǥ";
		chars[7][2] = "|-|";	chars[7][1] = "|=|";
		chars[8][2] = "!";	chars[8][1] = "1";
		chars[9][2] = "_|";	chars[9][1] = "_/";
		chars[10][2] = "|<";	chars[10][1] = "|{";
		chars[11][2] = " |_";	chars[11][1] = "|";
		chars[12][2] = "|\\/|";	chars[12][1] = "/X\\";
		chars[13][2] = "|\\|";	chars[13][1] = "И";
		chars[14][2] = "0";	chars[14][1] = "()";
		chars[15][2] = "|o";	chars[15][1] = "|>";
		chars[16][2] = "O_";	chars[16][1] = "(,)";
		chars[17][2] = "|2";	chars[17][1] = "Я";
		chars[18][2] = "$";	chars[18][1] = "5";
		chars[19][2] = "']['";	chars[19][1] = "'|'";
		chars[20][2] = "|_|";	chars[20][1] = "\\_\\";
		chars[21][2] = "\\/";	chars[21][1] = "ṽ";
		chars[22][2] = "Ш";	chars[22][1] = "\\X/";
		chars[23][2] = "Ж";	chars[23][1] = "}{";
		chars[24][2] = "\\|/";	chars[24][1] = "Ч";
		chars[25][2] = "Ƶ";	chars[25][1] = "ɀ";

	}
	
	
	public String Leet(String text){
		String start;
		String end;
		for(int i = 0; i < text.length(); i++){
			Random r = new Random();
			for(int k = 0; k < 25; k++){
				if(i  < text.length()){
					if(text.substring(i, i+1).equals(this.chars[k][0])) {
						start = text.substring(0,i);
						end = text.substring(i+1, text.length());
						String temp =  this.chars[k][r.nextInt(3)];
						text = start + temp + end;
						i += temp.length();
					}
				}
			}
		}
		return text;
	}
	
	public static void main(String args[]) {
		 BufferedReader reader =  new BufferedReader(new InputStreamReader(System.in)); 
         String text = "haxor";
         while(true) {
        	 try {
        		 text = reader.readLine();
        	 } catch (IOException e) {
        		 e.printStackTrace();
        	 } 
        	 LeetCipher cipher = new LeetCipher();
        	 String output = cipher.Leet(text);
        	 System.out.println(output);
	}
	}
	
	
}