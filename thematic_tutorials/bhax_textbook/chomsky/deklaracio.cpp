#include <stdio.h>

int w = 9;

int* h(){
	printf("Egészre mutató mutatót visszaadó függvény vagyok");
	return &w;
}

int* l(){
	printf("Én is egy egészre mutató mutatót visszaadó függvény vagyok");
	return &w;
}

int x (int c, int k){
	printf("Én egy sima egészet adok, és itt a két argumentumom összege: %d", c+k);
	return 42;
 }

int (*v (int c)) (int a, int b){
	printf("Én egy függvényre mutató mutatót adok vissza, és van egy argumentumom: %d", c);
	return &x;
}

int main()
{
int a=6;
int *b = &a;
int& r = a;
int c[5];
int (&tr)[5] = c;
int *d[5];
int *h ();
int *(*l) ();
int (*(*z) (int)) (int, int);

return 0;
}
