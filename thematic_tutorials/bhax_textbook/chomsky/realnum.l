%{
  #include <stdio.h>
  #include <stdlib.h>
  #include <time.h>
  #include <ctype.h>

  int numbers = 0;
  int letter = 0;
  int space = 0;
  int dot=0;

%}
%%
[0-9] {space=0;}
[a-zA-Z] {letter++;} 
[.] {dot++;}
[ ] {if(letter==0 && space==0 && dot<=1 ){numbers++;space++;} letter=0;dot=0;}
\n {if(space==0 && numbers!=0){printf("Number of real numbers: %d\n",numbers+1);}else{printf("Number of real numbers: %d\n",numbers);} numbers=0;letter=0;dot=0;space = 0;}
%%
int 
main()
{
  yylex();
  return 0;
}

