import java.io.BufferedReader; 
import java.io.IOException; 
import java.io.InputStreamReader;
import codeanticode.eliza.*;

final String INIT_MSG="";
String msg=INIT_MSG;
String msgl = INIT_MSG;
String input = ""; 
String output = "Hi, I'm Eliza";
Eliza eliza;
int lines = 0;
int write = 0;


void setup() {
  background(0);
  fill(255);
  size(600, 400);
  
  eliza = new Eliza(this);
}

void draw() {
  fill(255,255,255);


  if (write == 1){
    text(output, 40, lines*40+40);
    text(input, 40, lines*40+20);
    write--;
        fill(0);
    text(msg, 40, 380);

  }

}


void keyPressed() {


  if (msg.equals(INIT_MSG)) {
    msg="";
  }


  if ( (key>='a' && key<='z') ||
    (key>='A' && key<='Z') ||
    (key>='0' && key<='9') || key == ' ' || key == '?' 
    ) {
    msg+=key;
      text(msg, 40, 380);

  }
  if (key == ENTER) {
    input = msg;
    output = eliza.processInput(input);
            fill(0,0,0);

    rect(0,360,600,50);

        msg=INIT_MSG;
    lines++;
    write++;
  }
    if (key == BACKSPACE) {
      if(msg.length() != 0)
          msg = msg.substring(0,msg.length()-1);
  }
}
void liza() {
}
