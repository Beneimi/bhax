public class Iphone extends Phone{
	double ios_version = 0;
	public Iphone(double s, int f, double v){
		super(s,f);
		this.ios_version = v;	
	}
	public void SetIosVersion(double version){
		this.ios_version = version;
	}

	public double GetIosVersion(){
		return this.ios_version;
	}
}
