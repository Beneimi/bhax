public class Node
{
	int data;
	int height;
	Node right;
	Node left;

	public Node(int a,int sz){
		data = a;
		height = sz + 1;
	}

	public int Epit(Node r, char[] be){

     		for(int i = 0; i < be.length; i++){
			//ha 0 es nincs left ag
			if(be[i]=='0'&& this.left == null )
			{
				be[i] = '4';
				this.left =  new Node(0,this.height);
				//System.out.println("New 0 Node");
				if( i < be.length-1)
					r.Epit(r,be);			
			}
			//ha 1 es nincs right ag
			else if(be[i]=='1'&& this.right == null)
			{
				be[i] = '4';
				this.right =  new Node(1,this.height);
				//System.out.println("New 1 Node ");

				if( i < be.length-1){

					r.Epit(r,be);	
				}		
			}
			//ha 0 es van left ag
			else if(be[i]=='0'&& this.left!= null )
			{
				//System.out.println("next Node to left");
				be[i] = '4';
				if( i < be.length-1){
	
					this.left.Epit(r,be);
				}
			}
			//ha 1 es van right ag
			else if(be[i]=='1'&& this.right!= null )
			{
				//System.out.println("next Node to right");
				be[i] = '4';
				if( i < be.length-1)
					this.right.Epit(r,be);
     			}
		}
		return 0;
	}
	
	public int Sum(int sum){
		
		int osszeg = sum;
		if(this.left != null){
			//System.out.println("left node");
			osszeg += this.left.Sum(sum);
		}

	
		if(this.right != null){
			//System.out.println("right node");

			osszeg += this.right.Sum(sum);

		}

		if(this.data == 1)
			osszeg ++;

		return osszeg;

	}

/*public static void main(String[] args){
	Node gyoker = new Node(3,-1);
	int siker = gyoker.Epit(gyoker,args[0].toCharArray());
	//int print = gyoker.left.height;
	int sum = gyoker.Sum(0);
	System.out.println("összeg: " + sum);

}*/
}








