
#include <iostream>
#include "std_lib_facilities.h"


void szabadit (int *);
void copy(int* , int*);

class osztaly{
	public:
	int* szam;

// Move assignment
	osztaly & operator=( osztaly && regi ){
          	cout << "Move assignment\n";
	  	szam = regi.szam;
          	regi.szam = nullptr;
	  	return *this;
     }

// Move constructor based on move assignment
	osztaly(osztaly && regi){
		cout << "Move ctor\n";
		*this = std::move(regi);
	}


// Move constructor
/*	osztaly(osztaly && regi): szam(regi.szam){
		cout << "Move ctor\n";
		regi.szam = nullptr;
	}
*/
//parameterized constructor
	osztaly(int* szam_int){
		szam = szam_int;
		cout << "osztaly param. ctor\n";
	}

//copy assignment
	osztaly & operator=( const osztaly &other) 
	{	
		free(this->szam);
		this->szam = other.szam;
		cout << "Copy assignment\n";
	}

//default constructor
	osztaly(): szam(nullptr){
		cout << "osztaly ctor\n";
		}
//destructor
	~osztaly(){
		free(szam);
		cout << "osztaly destructor\n";
	}

//copy ctor
	osztaly(const osztaly &regi)
	{
		szam = new int;
		this->szam = regi.szam;
		cout << "Copy ctor\n";
	}


};

int main(){

int a = 5;

osztaly teszt_0(&a);
osztaly teszt_1;

osztaly teszt_2;

//call copy constructor
osztaly teszt_4(teszt_2);

//call copy assignment
teszt_2 =  teszt_0;

//call move assignment (rvalue)
osztaly teszt_3 = std::move(teszt_0);

// call move ctor
osztaly teszt_5(std::move(teszt_3));
}
