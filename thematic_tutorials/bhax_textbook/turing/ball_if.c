#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int
main ( void )
{
    WINDOW *ablak;
    ablak = initscr ();

    int x = 0;
    int y = 0;

    int xnov = 1;
    int ynov = 1;

    int mx;
    int my;
    float speedm=1;
    char c;
    int clear = 0;

    while (1) {
	nodelay(stdscr, 1);
	if((c = getch()) == ERR){
        getmaxyx ( ablak, my , mx ); //ablak magassága és szélessége my és mx változókba

        mvprintw ( y, x, "O" ); // (x,y) koordinátákra megy és ír egy karaktert

        refresh ();		//kepernyo frissitese
        usleep ( 100000/speedm );	//program futasanak szuneteltetese
	
	if(clear==1){
	clear();		//console tartalmanak torlese
	}

        x = x + xnov;		//leptetes x menten
        y = y + ynov;		//leptetes y menten

        if ( x>=mx-1 ) { // elerte-e a jobb oldalt?
            xnov = xnov * -1; //visszafordul
        }
        if ( x<=0 ) { // elerte-e a bal oldalt?
            xnov = xnov * -1; //visszafordul
        }
        if ( y<=0 ) { // elerte-e a tetejet?
            ynov = ynov * -1; //visszafordul
        }
        if ( y>=my-1 ) { // elerte-e a aljat?
            ynov = ynov * -1; //visszafordul
        }
	}

	else{
	    clear();
	    refresh();
	    printf("Speed Multiplier:");
	    scanf("%f", &speedm);
	    clear();
	    refresh();
	    printf("Clear (1) or Not (0): ");
	    scanf("%d", &clear);
	    clear();
	    x=y=0;
	    xnov=ynov=1;
	    c=0;
	
	}

    }

    return 0;
}
