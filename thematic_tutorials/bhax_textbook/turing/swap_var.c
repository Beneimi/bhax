#include <stdio.h>

void read(int *a,int *b)
{
printf("A=");
scanf("%d",a);
printf("B=");
scanf("%d",b);
}

void swap(int *a,int *b)
{
*a= *a ^ *b;
*b= *a ^ *b;
*a= *a ^ *b;
}

void write(int a,int b)
{
printf("Megcserélve: A=%d, B=%d",a,b);
}

int main()
{
int a,b;
read(&a,&b);
swap(&a,&b);
write(a,b);
}
