#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int
main ( void )
{
    WINDOW *ablak;
    ablak = initscr ();

    int x = 0;
    int y = 0;

    int xnov = 1;
    int ynov = 1;

    int mx;
    int my;

    getmaxyx ( ablak, my , mx );

    int x_axis[mx];
    for(int i=0;i<mx;i++){
    x_axis[i]=i+1;
    //printf("%d ",x_axis[i]);
	}

    int y_axis[my];
    for(int i=0;i<my;i++){
    y_axis[i]=i+1;
    //printf("%d ",y_axis[i]);
	}
    endwin();

    while(1){
	mvprintw ( y_axis[y], x_axis[x], "O" );
	//printf("%d ",x_axis[x]);
        refresh ();
        usleep ( 150000 );
	x_axis[x] = mx-x_axis[x]+1;
	y_axis[y] = my-y_axis[y]+1;
	x++;
	y++;
	x=x%mx;
	y=y%my;
    }

    return 0;
}
