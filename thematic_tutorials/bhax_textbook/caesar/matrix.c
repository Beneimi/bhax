#include <stdio.h>
#include <stdlib.h>

int
main ()
{
    int nr = 7;
    int **tm;
    
    printf("&tm: %p\n", &tm);
    
    if ((tm = (int **) malloc (nr * sizeof (int *))) == NULL)
    {
        return -1;
    }

    printf("tm: %p\n", tm);
    
    for (int i = 0; i < nr; ++i)
    {
        if ((tm[i] = (int *) malloc ((i + 1) * sizeof (int))) == NULL)
        {
            return -1;
        }

    }

    printf("tm[0]: %p\n", tm[0]);    
    
    //for (int i = 0; i < nr; ++i)
        //for (int j = 0; j < i + 1; ++j)
           // tm[i][j] = i * (i + 1) / 2 + j;

    for (int i = 0; i < nr; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
	{
	    tm[i][j] = i * (i + 1) / 2 + j;
            printf ("%d, ", tm[i][j]);
	}
        printf ("\n");
    }

    tm[3][0] = 42.0;
    (*(tm + 3))[1] = 43.0;	// mi van, ha itt hiányzik a külső ()
    *(tm[3] + 2) = 44.0;
    *(*(tm + 3) + 3) = 45.0;

    for (int i = 0; i < nr; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf ("%d, ", tm[i][j]);
        printf ("\n");
    }

    for (int i = 0; i < nr; ++i)
        free (tm[i]);

    free (tm);

    return 0;
}

