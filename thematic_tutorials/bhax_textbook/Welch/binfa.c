#include <stdio.h>
#include <stdlib.h>


typedef struct elem elem;

 struct elem{
	int adat;
	int szint;
	elem* jobb;
	elem* bal;
};

elem* uj_elem(int a,int sz){
		elem* p = malloc(sizeof(elem));
		p->adat= a;
		p->bal=NULL;
		p->jobb=NULL;
		p->szint= sz+1;
		return p;
	}

//struct mukodik


elem* epit(char* be){
	
	elem* gyoker = uj_elem(3,-1);
	elem* aktualis = gyoker;

for(int i=0;i>-1;i++)
{
		if(be[i] == 'E')
			return gyoker;

		//ha 0 es nincs bal ag
		if(be[i]=='0'&& aktualis->bal==NULL)
		{
			aktualis->bal =  uj_elem(0,aktualis->szint);
			aktualis = gyoker;
			
		}
		//ha 1 es nincs jobb ag
			else if(be[i]=='1'&& aktualis->jobb==NULL)
		{
			aktualis->jobb = uj_elem(1,aktualis->szint);
			aktualis = gyoker;
			
		}
		//ha 0 es van bal ag
			else if(be[i]=='0'&& aktualis->bal!=NULL)
		{
			aktualis=aktualis->bal;
			
		}
		//ha 1 es van jobb ag
			else if(be[i]=='1'&& aktualis->jobb!=NULL)
		{
			aktualis=aktualis->jobb;
			
		}
	
}
}

char* beolvas (){
        char* string = (char*)malloc(256*sizeof(char));
	char buff = '0';
	int index = 0;
	while(buff != EOF ){
		buff = getchar();

		if(index%256 == 0 && index != 0)
			string = realloc(string,256*sizeof(char)*(int)(index/256));

		string[index] = buff;
		index++;
	}
	string[index+1] = 'E';
	return string;
}


void inorder(elem* fa, double* ertekek){
	if(fa != NULL){
		inorder(fa->bal, ertekek);
		printf("%d ", fa->adat);
		
		ertekek[1]+= fa->adat;
		ertekek[2]++;

		if(fa->szint > ertekek[3])
			ertekek[3] = fa->szint;	
		
		if(fa->jobb == NULL && fa->bal == NULL)
			ertekek[0]++;
		
		
		
		inorder(fa->jobb, ertekek);
	}
}

void preorder(elem* fa){
	if(fa != NULL){
		
		printf("%d ", fa->adat);

		preorder(fa->bal);
		
		preorder(fa->jobb);
	}
}

void postorder(elem* fa){
	if(fa != NULL){

		postorder(fa->bal);
		
		postorder(fa->jobb);

		printf("%d ", fa->adat);
	}
}


int main()
{

elem* fa = epit(beolvas());

double ertekek[4] = {0,0,0,0}; // agak szama, adatösszeg, node szám, alja

printf("Fa inorder bejárása:\n" );
inorder(fa,ertekek);

printf("\nFa preorder bejárása:\n" );
preorder(fa);

printf("\nFa postorder bejárása:\n" );
postorder(fa);

printf("\nágak száma: %d, adatok összege: %d, nodeok száma: %d, fa magassága: %d \n" , (int)ertekek[0],(int)ertekek[1]-3,(int)ertekek[2]-1,(int)ertekek[3]);


}
