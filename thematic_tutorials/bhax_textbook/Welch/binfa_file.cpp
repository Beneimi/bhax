#include "std_lib_facilities.h"
#include <iostream>
#include <fstream>

 class node{
	public:
	int data;
	int height;
	node* right;
	node* left;

	node(int a,int sz):data(a), left(nullptr), right(nullptr), height(sz+1){
		}
};

void szabadit (node *);
void copy(node* , node*);

class tree{
	public:
	node* root;
// Move assignment
	tree & operator=( tree && old ){
          	cout << "Move assignment\n";
	  	root = old.root;
          	old.root = nullptr;
	  	return *this;
     }

// Move constructor based on move assignment
	tree(tree && old){
		cout << "Move ctor\n";
		*this = std::move(old);
	}


// Move constructor
/*	tree(tree && old): root(old.root){
		cout << "Move ctor\n";
		old.root = nullptr;
	}
*/
//parameterized constructor
	tree(node* root_node){
		root = root_node;
		cout << "Tree param. ctor\n";
	}

//copy assignment
	tree & operator=( const tree &other) 
	{	
		szabadit(this->root);
		this->root = other.root;
		cout << "Copy assignment\n";
	}

//default constructor
	tree(): root(nullptr){
		cout << "Tree ctor\n";
		}
//destructor
	~tree(){
		szabadit(root);
		cout << "Tree destructor\n";
	}

//copy ctor
	tree(const tree &old)
	{
		root = new node(3,-1);
		copy(old.root,root);
		cout << "Copy ctor\n";
	}





};

void copy(node* fa1, node* fa2)
	{
	if(fa1 != nullptr){
		if( fa1->left != nullptr )
		fa2->left = new node(fa1->left->data,fa1->left->height);

		if( fa1->left != nullptr )
		fa2->right = new node(fa1->right->data,fa1->right->height);

		copy(fa1->left,fa2->left);
		
		copy(fa1->right,fa2->right);
	}
}


node* build_from_file(char* inFile){

std::fstream beFile(inFile, std::ios_base::in);

    if (!beFile)
    {
        std::cout << inFile << " nem letezik..." << std::endl;

    }

	unsigned char be;
	node* root = new node(3,-1);
	node* current = root;

while(beFile.read((char *) &be, sizeof (unsigned char)))
{	
	for(int i = 0; i < 8; i++){
			
		//ha 0 es nincs left ag
		if(!(be & 0x80) && current->left==nullptr)
		{
			current->left =  new node(0,current->height);
			current = root;
			
		}
		//ha 1 es nincs right ag
			else if( (be & 0x80) && current->right==nullptr)
		{
			current->right = new node(1,current->height);
			current = root;
			
		}
		//ha 0 es van left ag
			else if( !(be & 0x80) && current->left!=nullptr)
		{
			current=current->left;
		}
		//ha 1 es van right ag
			else if( (be & 0x80) && current->right!=nullptr)
		{
			current=current->right;
		}
		
		be <<= 1;
	}
	
}
return root;
}

node* build(vector<char> be){

	node* root = new node(3,-1);
	node* current = root;

for(int i=0;i<be.size();i++)
{
			
		//ha 0 es nincs left ag
		if(be[i]=='0'&& current->left==nullptr)
		{
			current->left =  new node(0,current->height);
			current = root;
			
		}
		//ha 1 es nincs right ag
			else if(be[i]=='1'&& current->right==nullptr)
		{
			current->right = new node(1,current->height);
			current = root;
			
		}
		//ha 0 es van left ag
			else if(be[i]=='0'&& current->left!=nullptr)
		{
			current=current->left;
		}
		//ha 1 es van right ag
			else if(be[i]=='1'&& current->right!=nullptr)
		{
			current=current->right;
		}
	
}
return root;
}




vector<char> read(){
        vector<char> input;

	char buff = '0';
	while(buff != EOF ){
		buff=getchar();
		input.push_back(buff);
	}
	return input;
}


void inorder(node* fa, double* values){
	if(fa != nullptr){
		inorder(fa->left, values);
		printf("%d ", fa->data);
		
		values[1]+= fa->data;
		values[2]++;

		if(fa->height > values[3])
			values[3] = fa->height;	
		
		if(fa->right == nullptr && fa->left == nullptr)
			values[0]++;
		
		inorder(fa->right, values);
	}
}

void preorder(node* fa){
	if(fa != nullptr){
		
		printf("%d ", fa->data);

		preorder(fa->left);
		
		preorder(fa->right);
	}
}

void postorder(node* fa){
	if(fa != nullptr){

		postorder(fa->left);
		
		postorder(fa->right);

		printf("%d ", fa->data);
	}
}

void szabadit (node *csomopont)
{
    if (csomopont != nullptr)
        {
            // postorder
            szabadit(csomopont->left);
            szabadit(csomopont->right);

            delete csomopont;
   }
}

int main(int argc, char **argv)
{



if(argc == 1){
	char* inFile = *++argv;
	tree fa(build_from_file(inFile));
}
else if(argc == 0){
tree fa(build(read()));
}
else{
cout << "Vagy adjon meg egy file-t vagy adjon meg futás közben kézzel inputot";
}

double values[4] = {0,0,0,0}; // agak szama, dataösszeg, node szám, alja

printf("Fa inorder bejárása:\n");
inorder(fa.root,values);

printf("\nFa preorder bejárása:\n" );
preorder(fa.root);

printf("\nFa postorder bejárása: \n" );
postorder(fa.root);

printf("\nÁgak száma: %d, adatok összege: %d, nodeok száma: %d, fa magassága: %d \n" , (int)values[0],(int)values[1]-3,(int)values[2]-1,(int)values[3]);

tree fa2;

tree fa3;

//call copy constructor
tree fa4(fa2);

//call copy assignment
fa2 =  fa;

//call move assignment (rvalue)
fa3 = std::move(fa);

// call move ctor
tree fa5(std::move(fa3));

}
