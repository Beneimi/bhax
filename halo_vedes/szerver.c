
#include "lib.h"


int main(int argc, char *argv[] ){ 	// arg count, arg vector 

//********** Initialize Game **************//
   srand(time(NULL));  

   int number_of_players = 0;

   sscanf(argv[1], "%d", &number_of_players);

   deck* d = new_deck();

   card* discard_pile;

   if(number_of_players <2 || number_of_players >4){
	printf("2, 3 or 4 players can play.\n");
	return -1;
   }

   printf("Number of players: %d\n",number_of_players);

   player* players[number_of_players];

   char names[4][16];
   
   card* starting_hand[5];


   /* Declarations */
   int fd;	        		// socket endpt
   int fdc[4];                        	// socket endpt
   int flags;                      	// rcv flags
   struct sockaddr_in server;      	// socket name (addr) of server
   struct sockaddr_in clients[4];	// socket name of client
   int server_size;                	// length of the socket addr. server
   int client_size;                	// length of the socket addr. client
   int bytes;		         	// length of buffer 
   int rcvsize;                    	// received bytes
   int trnmsize;                   	// transmitted bytes
   int err;                        	// error code
   char on;                        	// 
   char buffer[BUFSIZE+1];	     	// datagram dat buffer area   
   char ack_buffer[BUFSIZE+1];	     	// datagram dat buffer area   

   /* Initialization */
   on                     = 1;
   flags                  = 0;
   bytes                  = BUFSIZE;
   server_size            = sizeof server;
   client_size            = sizeof clients[0];
   server.sin_family      = AF_INET;
   server.sin_addr.s_addr = INADDR_ANY;
   server.sin_port        = htons(PORT_NO);

   /* Creating socket */
   fd = socket(AF_INET, SOCK_STREAM, 0 );
   if (fd < 0) {
      error("%s: Socket creation error\n",argv[0]);
      exit(1);
      }

   /* Setting socket options */
   setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char *)&on, sizeof on);
   setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, (char *)&on, sizeof on);

   /* Binding socket */
   err = bind(fd, (struct sockaddr *) &server, server_size);
   if (err < 0) {
      error("%s: Cannot bind to the socket\n",argv[0]);
      exit(2);
      }


//***************** get fdc-s and names *******************//

for(int i = 0; i < number_of_players; i++){

   /* Listening */
   err = listen(fd, 10);
   if (err < 0) {
      error("%s: Cannot listen to the socket\n",argv[0]);
      exit(3);
      }

   /* Accepting connection request */
   fdc[i] = accept(fd, (struct sockaddr *) &clients[i], &client_size);
   if (fdc[i] < 0) {
      error("%s: Cannot accept on socket\n",argv[0]);
      exit(4);
      }

   /* Receiving names from the clients */
   rcvsize = recv( fdc[i], buffer, BUFSIZE, flags );
   if (rcvsize < 0) {
      error("%s: Cannot receive from the socket\n",argv[0]);
      exit(5);
      }

   strcpy(names[i],buffer);


//************** draw starting hand ************// 
   for(int k = 0; k < 5; k++){
	starting_hand[k] = draw_from_deck(d);
	
	}


   /************** Sending starting hand to the players ***************/

      sprintf(buffer,"%d,%d.%d,%d.%d,%d.%d,%d.%d,%dx",starting_hand[0]->color, starting_hand[0]->value,starting_hand[1]->color, starting_hand[1]->value,starting_hand[2]->color, starting_hand[2]->value,starting_hand[3]->color, starting_hand[3]->value,starting_hand[4]->color, starting_hand[4]->value);

   bytes = strlen(buffer) + 1;

   trnmsize = send(fdc[i], buffer, bytes, flags);

   players[i] = new_player(names[i],fdc[i]);

   printf ("%d. Player joined: %s \n",i+1, players[i]->name);

  
   	 /* Recieve ACK from players */
   	 rcvsize = recv( fdc[i], buffer, BUFSIZE, flags );


}


   /************** Sending player names to the players ***************/

   //strcpy(buffer,""); //empty buffer

   sprintf(buffer,"%d,",number_of_players);

 //***** write names into buffer ****//

   for(int j = 0; j < number_of_players; j++){
   	strcat(buffer, names[j]);
	if(j < number_of_players-1)
   		strcat(buffer, ",");
   }



   for(int h = 0; h < number_of_players; h++){

  	 bytes = strlen(buffer) + 1;
   	 trnmsize = send(fdc[h], buffer, bytes, flags);

   }


card *drawn;

discard_pile = draw_from_deck(d);

char hand_size_str[2049]; 

int draw_counter = 1;

card* counter_card;

int give_up[number_of_players];

int remaining_players = number_of_players;

for(int i = 0; i < number_of_players; i++)
	give_up[i] = 0;

//************************ Game Loop ************************//
//***********************************************************//

for(int i = 0; i < 10; i++ ){


   i = i % number_of_players;
   if(remaining_players == 1){
	for(int f = 0; f < number_of_players; f++){
		if(give_up[f] == 0)
			give_up[0] = f;
	}
	break;
   }

   if(give_up[i])
	continue;

   printf("%s's Turn:\n%d cards in hand.\n",players[i]->name,players[i]->hand_size);


   memset(buffer, 0, sizeof(buffer)); //empty buffer

 //**** write handsize and player id into buffer ***//

   for(int j = 0; j < number_of_players; j++){
	   if(give_up[j])
		continue;

	sprintf(hand_size_str,"%d",players[j]->hand_size);
	

   	strcat(buffer,hand_size_str);



	if(j < number_of_players-1)
   		strcat(buffer, ",");
   }


   /* Sending handsize to players */

   for(int k = 0; k < number_of_players; k++){

   	if(give_up[k])
		continue;
   	bytes = strlen(buffer) + 1;
   	trnmsize = send(fdc[k], buffer, bytes, flags);

   	 /* Recieve ACK from players */
   	 rcvsize = recv( fdc[k], ack_buffer, BUFSIZE, flags );
   }

   for(int k = 0; k < number_of_players; k++){
   /* Sending discard_pile to player */
	   if(give_up[k])
		continue;
      	if(k != i){
   		sprintf(buffer,"%d,%dx",discard_pile->color,discard_pile->value);
   		bytes = strlen(buffer) + 1;
   		trnmsize = send(fdc[k], buffer, bytes, flags);
        }
   	else{
   		sprintf(buffer,"%d,%dy",discard_pile->color,discard_pile->value);
   		bytes = strlen(buffer) + 1;
   		trnmsize = send(fdc[k], buffer, bytes, flags);
   	}
   }


// **************** player's turn **************



   /* Recieve answer from player */
   rcvsize = recv( fdc[i], buffer, BUFSIZE, flags );



//**************** sending cards ****************//

   if(buffer[0] == 'd'){

	if(discard_pile->value > 9 && discard_pile->value < 97)
		draw_counter = (discard_pile->value) - 8;


	for(int e = 0; e < draw_counter; e++){

	   /* Sending new card to player */
		drawn = draw_from_deck(d);
   		sprintf(buffer,"%d,%dx",drawn->color,drawn->value);
   		bytes = strlen(buffer) + 1;
   		trnmsize = send(fdc[i], buffer, bytes, flags);

		players[i]->hand_size ++;

        	 /* Recieve ACK from players */
   		rcvsize = recv( fdc[i], ack_buffer, BUFSIZE, flags );

	}

	if(draw_counter > 1)
		discard_pile->value = BLANK;

	draw_counter = 1;
	
    } //****** if color switch  *****//
    else if(buffer[0] == 'c'){

	if(buffer[1] == 'r')
		discard_pile = new_card(RED,BLANK);
	if(buffer[1] == 'g')
		discard_pile = new_card(GREEN,BLANK);
	if(buffer[1] == 'b')
		discard_pile = new_card(BLUE,BLANK);
	if(buffer[1] == 'z')
		discard_pile = new_card(YELLOW,BLANK);

	players[i]->hand_size --;

    }   // ******* give up *********** //
    else if(buffer[0] == 'g'){
    	give_up[i] = 1;
	remaining_players --;
	printf("%s gave up\n",players[i]->name);
	continue;
    }
    else{

       	players[i]->hand_size --;

       //****** if plus to plus *******//

	if(discard_pile->value > 9 && discard_pile->value < 98){
		counter_card = get_discard_pile(buffer);
		discard_pile->value += (counter_card->value) -8;
	}
	else{	//*********** if not to plus *******// 
       		discard_pile = get_discard_pile(buffer);
	}
    }

   fflush(stdout);


// ************ Send winner name *********** //

   if(players[i]->hand_size == 0){

   	strcpy(buffer,""); //empty buffer
   	strcat(buffer, "Winner: ");
   	strcat(buffer, players[i]->name);

   	for(int k = 0; k < number_of_players; k++){
   		bytes = strlen(buffer) + 1;
   		trnmsize = send(fdc[k], buffer, bytes, flags);
	}

	printf("Winner: %s\n", players[i]->name);
	i = 10;
   }
   
}

   if(remaining_players == 1){
	strcpy(buffer,""); //empty buffer
   	strcat(buffer, "Winner: ");
   	strcat(buffer, players[give_up[0]]->name);

   	bytes = strlen(buffer) + 1;
   	trnmsize = send(fdc[give_up[0]], buffer, bytes, flags);
   }

   /* Closing sockets and quit */
for(int i = 0; i < number_of_players; i++)
   close(fdc[i]);

   close(fd);
   exit(0);
} 
