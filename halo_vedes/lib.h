#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h> 
#include <arpa/inet.h>
#include <string.h>
#include <math.h>


#define BUFSIZE 2048                      // buffer size
#define PORT_NO 2001                      // port number
#define error(a,b) fprintf(stderr, a, b)  // error 'function'

#define PRINT_WHITE "\x1B[37m"
#define PRINT_BLUE "\x1B[34m"
#define PRINT_YELLOW "\x1B[33m"
#define PRINT_GREEN "\x1B[32m"
#define PRINT_RED "\x1B[31m"

#define RED 0
#define GREEN 1
#define BLUE 2
#define YELLOW 3
#define WHITE 4

#define PLUS_TWO 10
#define COLOR_SWITCH 98
#define BLANK 99


typedef struct card card;

typedef struct player player;
typedef struct deck deck;

struct card {
	int color;
	int value;
};

card* new_card(int col,int val){
		card* p = malloc(sizeof(card));
		p->color = col;
		p->value = val;
		return p;
	}

void clear_stream(FILE *in)
{
    int ch;
    
    clearerr(in);
    
    do
        ch = getc(in);
    while (ch != '\n' && ch != EOF);
    
    clearerr(in);
}

//48-57
int ch_to_int(char* choice)
{

int number = 0;


if(choice[0] >=48 && choice[0] <=57){
	number += choice[0]-48;
}
if(choice[1] >=48 && choice[1] <=57){
	number *= 10;
	number += choice[1]-48;
}


return number-1;
}


int print_color(card* l){

	if(l->color == RED)
		printf(PRINT_RED);
	if(l->color == GREEN)
		printf(PRINT_GREEN);
	if( l->color == BLUE)
		printf(PRINT_BLUE);
	if( l->color == YELLOW)
		printf(PRINT_YELLOW);
	if( l->color == WHITE)
		printf(PRINT_WHITE);

}

void swap_cards(card* one, card* other){
if(one != other){
	int temp_color = one->color;

	int temp_value = one->value;

	one->color = other->color;
	one->value = other->value;

	other->color = temp_color;
	other->value = temp_value;
}
}


//*********** functions for the client ****************

card** play_card(card** hand,int *hand_size, int played_card){

			swap_cards(hand[*hand_size-1],hand[played_card]);
			(*hand_size)--;
			card** new_hand = (card**)malloc(sizeof(card*)*(*hand_size));
		
			for(int i = 0; i < *hand_size; i++)
				new_hand[i] = hand[i];

			free(hand);
			return new_hand;
}

card** get_card(char* buffer, card** hand,int *hand_size){

	int color = 0;
	int value = 0;
	int cv = 0;

	for(int i = 0;;i++){

		if(buffer[i] >= '0' && buffer[i] <= '9'){
			if(cv == 0)
				color = buffer[i] -'0';
			else if(value == 0)
				value = buffer[i] - '0';
			else{
				value *=10;
				value += buffer[i] - '0';
			}
	
		}

		if (buffer[i] == ',')
			cv = 1;

		if(buffer[i] == '.'){
			cv = 0;

			(*hand_size)++;

			hand = realloc(hand,*hand_size * sizeof(card*));

			hand[*hand_size-1] = new_card(color,value);
			color = 0;
			value = 0;
			cv = 0;
		}
		if(buffer[i] == 'x'){
			(*hand_size)++;
			hand = realloc(hand,*hand_size * sizeof(card*));
			hand[*hand_size-1] = new_card(color,value);
			return hand;
		}

	}

}

card* get_discard_pile(char* buffer){

	int color = 0;
	int value = 0;
	int cv = 0;

	for(int i = 0;;i++){

		if(buffer[i] >= '0' && buffer[i] <= '9'){
			if(cv == 0)
				color = buffer[i] -'0';
			else if(value == 0)
				value = buffer[i] - '0';
			else{
				value *=10;
				value += buffer[i] - '0';
			}
		}

		if (buffer[i] == ',')
			cv = 1;


		if(buffer[i] == 'x' || buffer[i] == 'y'){
			return new_card(color,value);
		}
	}
}



int match (card** hand,int hand_size, card* discard_pile, int choice)
{

if(choice+1 <= hand_size  &&  choice+1 > 0){


   if(discard_pile->value < 10 || discard_pile->value > 97){

	if( (hand[choice]->color == discard_pile->color) || (hand[choice]->value == discard_pile->value ) || (hand[choice]->color == WHITE) || (discard_pile->color == WHITE) )
	{
		return 0;
	}
	else
	{	
		printf("This doesn't match, chose another one: ");

	}
   }
   else{
	if( hand[choice]->value == PLUS_TWO ){
		return 0;
	}
	else
	{	
		printf("This doesn't match, chose another one: ");

	}

   }
}
else{

	printf("you have chosen: %d, chose a card between 0 and %d : ", choice, hand_size);
}
return 1;
}

card* draw_card(card* old){
	return realloc(old,sizeof(old)+sizeof(card));
}




void draw(card** hand,int hand_size, card* discard_pile)
{

int row = 7;

if(row > hand_size)
	row = hand_size;

system("clear");
print_color(discard_pile);

// **** if number *****//

if(discard_pile->value < 10 && discard_pile->value >= 0){
	printf("                      _______\n                      |%d    |\n                      |     |\n                      |    %d|\n                      -------\n\n",discard_pile->value,discard_pile->value);
}

// **** if + *****//
if(discard_pile->value > 9 && discard_pile->value < 98){
	if(discard_pile->value < 17 ){
	printf("                      _______\n                      |+%d   |\n                      |     |\n                      |   +%d|\n                      -------\n\n",discard_pile->value -8, discard_pile->value -8);
	}
	else{
	printf("                      _______\n                      |+%d  |\n                      |     |\n                      |  +%d|\n                      -------\n\n",discard_pile->value -8, discard_pile->value -8);
	}

}

// **** if color switch *****//

if(discard_pile->value == COLOR_SWITCH){
	printf("                      _______\n                      |C    |\n                      |     |\n                      |    C|\n                      -------\n\n");
}

// **** if blank *****//

if(discard_pile->value == BLANK){
	printf("                      _______\n                      |     |\n                      |     |\n                      |     |\n                      -------\n\n");
}

for(int k = 0; k > -1; k++){

	for(int i = 0; i < row; i++){
		print_color(hand[((k*7)+i)]);
		printf("  _______ ");
	}
	printf("\n");

	for(int i = 0; i < row; i++){
		print_color(hand[((k*7)+i)]);
		if(hand[(k*7)+i]->value < 10 && hand[(k*7)+i]->value >= 0)
			printf("  |%d    | ",hand[(k*7)+i]->value);

		if(hand[(k*7)+i]->value == PLUS_TWO)
			printf("  |+2   | ");

		if(hand[(k*7)+i]->value == COLOR_SWITCH)
			printf("  |C    | ");
	

	}
	printf("\n");

	for(int i = 0; i < row; i++){
		print_color(hand[(k*7)+i]);
		printf("  |     | ");
	}

	printf("\n");

	for(int i = 0; i < row; i++){
		print_color(hand[(k*7)+i]);
		if(hand[(k*7)+i]->value < 10)
			printf("  |    %d| ",hand[(k*7)+i]->value);

		if(hand[(k*7)+i]->value == PLUS_TWO)
			printf("  |   +2| ");

		if(hand[(k*7)+i]->value == COLOR_SWITCH)
			printf("  |    C| ");


	}
	printf("\n");

	for(int i = 0; i < row; i++){
		print_color(hand[(k*7)+i]);
		printf("  ------- ");
	}
	printf("\n");

	for(int i = 0; i < row; i++){
		printf(PRINT_WHITE);
		if((k*7)+i < 10)
			printf("     %d    ",(k*7)+i+1);
		else
			printf("    %d    ",(k*7)+i+1);
	}

	printf("\n");

	hand_size -= 7;

	if (hand_size < 7){
		row = hand_size;
	}
	if(hand_size < 0)
		k = -2;
}

}


//******************* functions for the server ********************

struct player{
	char name[16];
	int hand_size;
	int fd;
	
};

player* new_player(char player_name[16],int file_d){
		player* p = malloc(sizeof(player));
		strcpy(p->name,player_name);
		p->hand_size=5;
		p->fd = file_d;
		return p;
}


struct deck{
	card* cards[52];
	int top;
};

deck* new_deck(){
	deck* d = malloc(sizeof(deck));

	for(int i = 0; i<4 ;i++){
		d->cards[i*11] = new_card(i,PLUS_TWO);
		d->cards[i*11+1] = new_card(i,PLUS_TWO);
		for(int k = 2; k< 11; k++ )		
		d->cards[i*11+k] = new_card(i,k-1);		
	}
	for(int i=0; i<4;i++){
		d->cards[44+(1*i)] = new_card(WHITE,COLOR_SWITCH);
	}
	
	d->top = 47;
	return d;
}



card* draw_from_deck(deck *d){
	if(d->top == 0)
		d->top = 47;

	int random = rand() % (d->top+1);
	swap_cards(d->cards[d->top],d->cards[random]);
	d->top--;
	return d->cards[d->top+1];


}




