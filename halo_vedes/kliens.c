

#include "lib.h"


int main(int argc, char *argv[] ) {// arg count, arg vector   

   /* Declarations */
   int fd;	                       // socket endpt	
   int flags;                      // rcv flags
   struct sockaddr_in server;	     // socket name (addr) of server 
   struct sockaddr_in client;	     // socket name of client 
   int server_size;                // length of the socket addr. server 
   int client_size;                // length of the socket addr. client 
   int bytes;    	                 // length of buffer 
   int rcvsize;                    // received bytes
   int trnmsize;                   // transmitted bytes
   int err;                        // error code
   int ip;			// ip address
   char on;                        // 
   char buffer[BUFSIZE+1];         // datagram dat buffer area
   char server_addr[16];           // server address	

   /* Initialization */
   on    = 1;
   flags = 0;
   server_size = sizeof server;
   client_size = sizeof client;
   sprintf(server_addr, "%s", argv[1]);
   ip = inet_addr(server_addr);
   server.sin_family      = AF_INET;
   server.sin_addr.s_addr = ip;
   server.sin_port        = htons(PORT_NO);
   


   /* Creating socket */
   fd = socket(AF_INET, SOCK_STREAM, 0);
   if (fd < 0) {
      error("%s: Socket creation error.\n",argv[0]);
      exit(1);
   }

   /* Setting socket options */
   setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char *)&on, sizeof on);
   setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, (char *)&on, sizeof on);

   /* Connecting to the server */
   err = connect(fd, (struct sockaddr *) &server, server_size);
   if (err < 0) {
      error("%s: Cannot connect to the server.\n", argv[0]);
      exit(2);
   }

   /* Sending my name to the server */

   strcpy(buffer,argv[2]);

   bytes = strlen(buffer)+1;

   trnmsize = send(fd, buffer, bytes, flags);




   /********** Receive starting hand from the server *************/
   
   rcvsize = recv( fd, buffer, BUFSIZE, flags );
   if (rcvsize < 0) {
      error("%s: Cannot receive data from the socket.\n", argv[0]);
      exit(4);
   }
/******** Initialize Game ********/

card* discard_pile;

int hand_size = 0;

card** hand = (card**) malloc(hand_size*sizeof(card*));

hand = get_card(buffer,hand, &hand_size);

int draw_counter = 1;

char color_choice;

int whose_turn = 0;

int my_id;

   /* Send ACK to the server */

   sprintf(buffer,"ACK");

   bytes = strlen(buffer) + 1;
   trnmsize = send(fd, buffer, bytes, flags);





   /* Receive player names from the server */
   
   rcvsize = recv( fd, buffer, BUFSIZE, flags );
   if (rcvsize < 0) {
      error("%s: Cannot receive data from the socket.\n", argv[0]);
      exit(4);
   }


//*************** get player names ****************//

int number_of_players = (int)buffer[0] -'0';

char names[number_of_players][16];

char* token = strtok(buffer, ","); 
  
for(int i = 0; i < number_of_players+1;i++){
	if(token == NULL)
		break;

		if(i != 0){
			sprintf(names[i-1],"%s",token); 
        		token = strtok(NULL, ","); 
			if(strcmp(argv[2],names[i-1]) == 0)
				my_id = i-1;
		}
		else
			token = strtok(NULL, ","); 

}

int hands[number_of_players];

//************************* Game Loop ************************//
//************************************************************//
while(1){


while(buffer[strlen(buffer)-1] != 'y'){

   whose_turn =  whose_turn % number_of_players;

	/* recieve handsize or end of game*/
   rcvsize = recv( fd, buffer, BUFSIZE, flags );

	if(buffer[0] == 'W')
		break;


	// *** get handsize **//

	token = strtok(buffer, ",");

	for(int i = 0; i < number_of_players;i++){
		if(token == NULL)
			break;

		hands[i] = atoi(token); 
       	 	token = strtok(NULL, ","); 
	
	}

   /* Send ACK to the server */

   sprintf(buffer,"ACK");

   bytes = strlen(buffer) + 1;
   trnmsize = send(fd, buffer, bytes, flags);



	/* recieve discard pile*/

   rcvsize = recv( fd, buffer, BUFSIZE, flags );



   discard_pile = get_discard_pile(buffer);

   draw(hand,hand_size,discard_pile);



   for(int k = 0; k < number_of_players; k++){
	if( k != my_id)
		printf("%s has %d cards\n",names[k],hands[k]);
   }

   printf("You have %d cards\n",hands[my_id]);

   if(whose_turn != my_id)
	printf("%s's Turn\n",names[whose_turn]);


   whose_turn++;
}

// ************ my turn ***********

	if(buffer[0] == 'W')
		break;

   //discard_pile = get_discard_pile(buffer);

   //draw(hand,hand_size,discard_pile);

   printf("Your Turn!\nType a number to pick a card, press 'd' to draw a card or 'g' to give up\n");

   int chosen_card = 0;

   char* answer = malloc(10*sizeof(char));

   do {
	chosen_card = 0;
	scanf("%s",buffer);
	clear_stream(stdin);
	chosen_card = ch_to_int(buffer); // index in hand 

		if(strcmp(buffer, "g") == 0){
			break;
		}
		else if(strcmp(buffer,"d") == 0){
			break;
		}
	
   }
   while(match(hand,hand_size, discard_pile,chosen_card));

// ********* get color choice if colorswitch **********//
if(buffer[0] != 'd'){

   if(buffer[0] == 'g'){
	sprintf(buffer,"g");
   	bytes = strlen(buffer) + 1;
   	trnmsize = send(fd, buffer, bytes, flags);
	break;
   }
   else if(hand[chosen_card]->value == COLOR_SWITCH){
	printf("Pick a color: r (red), g (green), b (blue), y (yellow) :");
	scanf(" %c",&color_choice);

	while(color_choice != 'r' && color_choice != 'g' && color_choice != 'b' && color_choice != 'y'){

	printf("Invalid color. Type 'r' (red), 'g' (green), 'b' (blue) or 'y' (yellow) :");
	scanf(" %c",&color_choice);

   	}
	if(color_choice == 'y')
		color_choice = 'z';	
	
	sprintf(buffer,"c%c",color_choice);
   }

}


	/***** send chosen card or give up or draw *****/
   if(buffer[0] != 'd' && buffer[0] != 'c')
   	sprintf(buffer,"%d,%dx",hand[chosen_card]->color,hand[chosen_card]->value);

   bytes = strlen(buffer) + 1;
   trnmsize = send(fd, buffer, bytes, flags);


 //**** discard chosen card ****//
   if(chosen_card < hand_size && chosen_card >= 0)
   	hand = play_card(hand, &hand_size, chosen_card);




// ************** draw card(s) *****************//


   if(buffer[0] == 'd'){

	if(discard_pile->value > 9 && discard_pile->value < 97)
		draw_counter = (discard_pile->value) - 8;

	for(int i = 0; i < draw_counter; i++){

		/*recive 'n' drawn card*/
		rcvsize = recv( fd, buffer, BUFSIZE, flags );


		hand = get_card(buffer,hand, &hand_size);

   		/* Send ACK to the server */

   		sprintf(buffer,"ACK");

   		bytes = strlen(buffer) + 1;
   		trnmsize = send(fd, buffer, bytes, flags);

	}
	if(draw_counter > 1)
		discard_pile->value = BLANK;

	draw_counter = 1;
   }





   draw(hand,hand_size,discard_pile);
   fflush(stdout);
}

if(buffer[0] == 'W'){
	system("clear");
	printf(PRINT_WHITE);
	printf("\n\n//***************** %s *****************//\n\n\n",buffer);
}
else
		printf("\n\n//***************** You gave up =( *****************//\n\n\n");

   /* Closing sockets and quit */
   close(fd);
   exit(0);
} 
